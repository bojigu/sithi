<?php session_start(); ?>
<html>

<head>
  <meta charset="utf-8">
  <title>SITHI-Sequence-based Integrated TM Homodimer Interface</title>
  <link rel="stylesheet" href="SITHI.css">
</head>
<body>
<div id="container">

<div id="logo">
<table>
<tr>
<td><h1>SITHI<h1></td>
</tr>
<tr>
<td><h2>Sequence-based Integrated TM Homodimer Interface</h2></td>
</tr>
</table>
</div>

<hr />
<div id="menu">

<ul>
<li><a href="SITHI.php" id="current" >Home</a></li>
<li><a href="SITHI_help.php">Help</a></li>
<li><a href="SITHI_contact.php">Contact</a></li>
<li><a  href="SITHI_download.php">Download</a></li>
</ul>

</div>

<hr />
<div id="main">
<p>Download local self installing SITHIpy python module <a href="SITHIpy.zip">here</a></p>
</div>
</body>
</html>
