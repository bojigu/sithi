<?php session_start(); ?>

<html>

<head>
  <meta charset="utf-8">
  <title>SITHI-Sequence-based Integrated TM Homodimer Interface</title>
  <link rel="stylesheet" href="SITHI.css">
</head>
<body>
<div id="container">

<div id="logo">
<table>
<tr>
<td><h1>SITHI<h1></td>
</tr>
<tr>
<td><h2>Sequence-based Integrated TM Homodimer Interface</h2></td>
</tr>
</table>
</div>

<hr />
<div id="menu">

<ul>
<li><a href="SITHI.php" id="current" >Home</a></li>
<li><a href="SITHI_help.php">Help</a></li>
<li><a href="SITHI_contact.php">Contact</a></li>
<li><a  href="SITHI_download.php">Download</a></li>
</ul>

</div>

<hr />
<div id="main">
<h2>About</h2>
<p>Proteins with a single transmembrane domain make up ~30% of all membrane proteins. 
Their interaction and oligomerisation in the membrane is vital for many biological processes. 
Due to a lack of crystal structures, their interaction interfaces are poorly understood. 
Here we present SITHI (Sequence-based Integrated Transmembrane Homodimer Interface), 
a algorithm which can predict interfacial residues from sequence data alone, with an accuracy of AUC 0.81 in the ROC curve.
 By objectively listing the most important input variables, 
 we find that residue co-evolution, hydrophobicity, conservation, phosphorylation and residue relative position in the sequence are the most important factors.</p>

</div>
</body>
</html>
