<?php session_start(); ?>

<html>

<head>
  <meta charset="utf-8">
  <title>SITHI-Sequence-based Integrated TM Homodimer Interface</title>
  <link rel="stylesheet" href="SITHI.css">
</head>
<body>
<div id="container">

<div id="logo">
<table>
<tr>
<td><h1>SITHI<h1></td>
</tr>
<tr>
<td><h2>Sequence-based Integrated TM Homodimer Interface</h2></td>
</tr>
</table>
</div>

<hr />
<div id="menu">

<ul>
<li><a href="SITHI.php" id="current" >Home</a></li>
<li><a href="SITHI_help.php">Help</a></li>
<li><a href="SITHI_contact.php">Contact</a></li>
<li><a  href="SITHI_download.php">Download</a></li>
</ul>

</div>

<hr />
<div id="main">
<h2>Contact</h2>
<p>This server is maintaned by Bo Zeng (zeng(at)wzw.tum.de in case of any problems please contact him or for feedback please contact him.<br>
The webserver is hosted by the Wissenshafts Zentrum Weihenstephan in the lab of <a href="http://frishman.wzw.tum.de/">Dmitrij Frishman</a>.
</p>
<h2>Cooperation</h2>
<p>This research was cooperated with the lab of Professor <a href="http://cbp.wzw.tum.de/index.php?id=10/">Dieter Langosch</p></p>
</div>
</body>
</html>
