<?php
ob_start();
session_start(); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>


<head>
<link rel="stylesheet" type="text/css" href="SITHI.css"/>

<title>SITHI-Sequence-based Integrated TM Homodimer Interface</title>
<meta http-equiv="Content-Type" content="text/html"; charset="UTF-8"">

<script type="text/javascript">
	function clearText(field){
	    if (field.defaultValue == field.value) field.value = '';
	    else if (field.value == '') field.value = field.defaultValue;
	}

	function example1(){
		document.forms["SITHI"]["fastaText"].value = ">sp|P02724|GLPA_HUMAN Glycophorin-A OS=Homo sapiens GN=GYPA PE=1 SV=2\nMYGKIIFVLLLSEIVSISASSTTGVAMHTSTSSSVTKSYISSQTNDTHKRDTYAATPRAH\nEVSEISVRTVYPPEEETGERVQLAHHFSEPEITLIIFGVMAGVIGTILLISYGIRRLIKK\nSPSDVKPLPSPDTDVPLSSVEIENPETSDQ";
	}
	function example2(){
		document.forms["SITHI"]["tmText"].value = "ITLIIFGVMAGVIGTILLISYGI";
	}
</script>

<script type="text/javascript">
 function show(id){
        document.getElementById(id).style.display='block';
    }
    function hide(id){
        document.getElementById(id).style.display='none';
    }
</script>



</head>

<body>
<?php

        function checkInput($text){
                $out = "";
                $line_number = 1;
		$lines = explode("\n", $text);
                foreach($lines as $line) {
			if($line == ""){
				continue;
			}
                        if(!preg_match("/^[A-Za-z0-9~`!#$%^&*()_+-]+[\s\t][A-Za-z0-9~`!#$%^&*()_+-]+[\n\s]?$/", $line)){
                                $out = "<span class=\"error\">Error in parsing input at line $line_number. Line: \"$line\" </span>";
                        }
			$line_number = $line_number + 1;
                }
                return $out;
        }


$featureErr = $textErr = $fileErr = "";
if ($_SERVER["REQUEST_METHOD"] == "POST"){
	// Make ID
	$id = -1;
	$features = "";
	$err = 0;
	try{
		$id = getUniqID();
	}catch(Exception $e){
		$err = 1;
		echo 'Message: ' .$e->getMessage();
	}
	$_SESSION['id'] = $id;
	mkdir("./out/$id", $mode = 0777);
	@chmod("./out/$id", 0777);
	// Create Value for Feature selection
	if(isset($_POST["tm_start"]) && isset($_POST["tm_end"])){
		$tm_start_end = implode ( "\t" , array($_POST["tm_start"],$_POST["tm_end"]));
		$_SESSION{'tmpos'}=$tm_start_end;
	}
	if(isset($_POST["email"])){
		$_SESSION{'email'}=$_POST["email"];
	}
	
	if(!(isset($_POST["fastaText"]) && $_POST["fastaText"] != "") && !(is_uploaded_file($_FILES['fastaFile']['tmp_name']))){
		$textErr = "<span class=\"error\"> Please add fasta sequence in the text field or supply a file for upload.</span>";
		$err = 1;
	}
	if((isset($_POST["fastaText"]) && $_POST["fastaText"] != "") && (is_uploaded_file($_FILES['fastaFile']['tmp_name']))){
                $textErr = "<span class=\"error\"> You have both uploaded a file, and entered fasta sequence. Please provide only one entry point.</span>";
                $err = 1;
        }
	// Check text
	if(isset($_POST["fastaText"]) && $_POST["fastaText"] != ""){
		$ppiText = $_POST["fastaText"];
		$lines = count(explode("\n", $ppiText));
		if($lines<=1000){
			file_put_contents("./out/$id/QuePro.fasta", $ppiText);
			@chmod("./out/$id/QuePro.fasta", 0777);
		}else{
			$textErr = "<span class=\"error\"> Textfield has more than 1000 lines (one interaction per line)</span>";
			$err = 1;
		}
		$validateText = checkInput($ppiText);
		if($validateText != ""){
			$textErr = $validateText;
			$err = 1;
		}
	}
	// Check File
	if(is_uploaded_file($_FILES['fastaFile']['tmp_name'])){
		if ($_FILES["fastaFile"]["size"] < 1048576){
			if(wc_l($_FILES['fastaFile']['tmp_name']) == 0){
				$fileErr = "<span class=\"error\">Selected file is empty, please try again!</span>";
				$err = 1;
			}elseif(wc_l($_FILES['fastaFile']['tmp_name']) <= 1000 && wc_l($_FILES['fastaFile']['tmp_name']) > 0){
				$fileText = file_get_contents($_FILES['fastaFile']['tmp_name']);
				$validateText = checkInput($fileText);
				if($validateText != ""){
					$fileErr = $validateText;
					$err = 1;
				}
				if(move_uploaded_file($_FILES['fastaFile']['tmp_name'], "./out/$id/QuePro.fasta")){
					@chmod("./out/$id/QuePro.fasta", 0777);
				} else {
			    		 $fileErr = "<span class=\"error\">There was an error uploading the file, please try again!</span>";
					$err = 1;
				}
			
			}else{
				$fileErr =  "<span class=\"error\">*File has to many lines (max 1000)</span>";
				$err = 1;
			}
		}else{
			$fileErr =  "<span class=\"error\"> File size is to large (max 1MB)</span>";
			$err = 1;
		}
	}
	header("Location: SITHI_submit.php");
	die();
}

function wc_l($file){
$linecount = 0;
$handle = fopen($file, "r");
while(!feof($handle)){
  $line = fgets($handle, 4096);
  $linecount = $linecount + substr_count($line, PHP_EOL);
}

fclose($handle);

return $linecount;
}

function getUniqID(){
	$i = 0;
	$uniqID = 0;
	while($i < 1000){
		$i++;
		$uniqID = uniqid();
		$filename = "./out/$uniqID";
		if (!file_exists($filename)) {
			return $uniqID;
		}
	}
	throw new Exception("Can not create random uniqID");
	return null;
}

function test_input($data)
{
     $data = stripslashes($data);
     $data = htmlspecialchars($data);
     return $data;
}
?>


<div id="container">

<div id="logo">
<table>
<tr>
<td><h1>SITHI<h1></td>
</tr>
<tr>
<td><h2>Sequence-based Integrated TM Homodimer Interface</h2></td>
</tr>
</table>
</div>

<hr />
<div id="menu">

<ul>
<li><a href="SITHI.php" id="current" >Home</a></li>
<li><a href="SITHI_help.php">Help</a></li>
<li><a href="SITHI_contact.php">Contact</a></li>
<li><a  href="SITHI_download.php">Download</a></li>
</ul>

</div>

<hr />


<div id="content">
<h3>Welcome to SITHI!</h3>
<p>SITHI (Sequence-based Integrated Transmembrane Homo-dimer Interface) is a web server developed for the interface prediction of trans-membrane protein homodimers.</p>
<form name = "SITHI"  method="post" action="<?php echo $_SERVER["PHP_SELF"]?>" enctype="multipart/form-data"> 
   <br>
   Please paste TM protein full sequence below in fasta format (<a href = "javascript:example1()" >Example</a>)
   <br>
   <?php echo $textErr;?> <br><textarea name="fastaText" id="fastaText" rows="10" cols="80"></textarea>
   <br>
   or upload a file: 
   <input type="file" name="fastaFile"> <?php echo $fileErr;?>
   <br><br>
   Please paste TM segment sequence below (<a href = "javascript:example2()" >Example</a>)
   <br>
   <?php echo $textErr;?> <br><textarea name="tmText" id="tmText" rows="5" cols="80"></textarea>
   <br><br>
   Or give the TMD start and end positions below and click submit:
   <br><br>
   Input: &nbsp;&nbsp;TM start:
	<input type="text" name="tm_start" value="92" size="6">
    &nbsp;&nbsp;&nbsp;&nbsp;TM end:
	<input type="text" name="tm_end" value="114" size="6">
	<br><br>
	
   Email: <input name="email" type="text" /><br />
   <br>
   
   <input type="submit" name="submit" value="Submit"/> 
   <br><br>

</form>

<p>If you wish to cite SITHI, please refer to our publication:</p>
<p>Xiao,Y., Zeng,B., Frishman,D., Langosch,D., Teese,M.-G. Predicting the role of single amino acids in transmembrane domain homodimers. (will submit soon)</p>


</div>

<div id="footer">
<table>
<tr>
<th><h4>Copyright&copy;Frishman and Langosch's Lab, &nbsp;All rights reserved<h4></th>
</tr>
</table>
</div>


</div>

</body>
</html>

